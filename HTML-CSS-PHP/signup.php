<?php
if(isset($_POST['reg-submit'])){
	
	require 'dbconnection.php';
	
	$username = $_POST['Username'];
	$password = $_POST['Password'];
	$email = $_POST['Email'];
	
	
	If(empty($username) || empty($password) || empty($email)) {
		header("Location: Login.php?error=emptyfields&Username=".$username."&email=".$email);
		exit();
	}

	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		header("Location: Login.php?error=ERROR");
		exit();
		
	}
	
	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		header("Location: Login.php?error=Invalidemail&UserName=".$username);
		exit();
		
	}
	
	elseif(!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		header("Location: Login.php?error=InvalidLogin&email=".$email);
		exit();
		
	}
	
	else {
		
		$sql = "SELECT Username FROM enduser WHERE Username=?";
		$stmt = mysqli_stmt_init($conn);
		if(!mysqli_stmt_prepare($stmt, $sql)){
			header("Location: Login.php?error=SQLERROR");
			exit();
		}
		
		else{
			mysqli_stmt_bind_param($stmt, "s", $username);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_store_result($stmt);
			$resultCheck = mysqli_stmt_num_rows($stmt);
			if ($resultCheck > 0){
				header("Location: Login.php?error=taken email");
			}
			
			else{
				$sql = "INSERT INTO EndUser (Username, Password, Email) VALUES (?,?,?)";
				$stmt = mysqli_stmt_init($conn);
				if(!mysqli_stmt_prepare($stmt, $sql)){
					header("Location: Login.php?error=SQLERROR");
					exit();
				
				}
			else{
				$hashedPassword = password_hash($password, PASSWORD_DEFAULT);
				
				
			mysqli_stmt_bind_param($stmt, "sss", $username, $hashedPassword, $email);
			mysqli_stmt_execute($stmt);
			header("Location: Login.php?signup=sucess");
					exit();
			
			
				}
			}	
	
		}

	}
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
	
	
}


else{
	header("Location: index.html");
}
	

